export interface ICountry {
    id: number;
    name: string;
    code: string;
    currency: string;
    capitalCity: string;
}