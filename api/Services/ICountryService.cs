public interface ICountryService
{
    IEnumerable<Country> GetAllCountries();
    Country GetCountryById(int id);
}