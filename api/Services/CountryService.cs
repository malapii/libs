public class CountryService : ICountryService {
    private
    readonly ICountryRepository _countryRepository;
    public CountryService(ICountryRepository countryRepository){
        _countryRepository = countryRepository;
    }
    public IEnumerable<Country> GetAllCountries() => _countryRepository.GetAllCountries();
    public Country GetCountryById(int id) => _countryRepository.GetCountryById(id);
}