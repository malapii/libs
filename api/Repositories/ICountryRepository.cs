public interface ICountryRepository
{
    IEnumerable<Country> GetAllCountries();
    Country GetCountryById(int id);
    // Add other methods for CRUD operations as needed
}