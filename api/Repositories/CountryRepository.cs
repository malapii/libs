public class CountryRepository : ICountryRepository
{
    private readonly List<Country> _countries = new List<Country>()
    {
        new Country { Id = 1, Name = "United States", Code = "US", Currency = "USD", CapitalCity = "Washington DC" },
        new Country { Id = 2, Name = "Taiwan", Code = "TW", Currency = "TUS", CapitalCity = "Taipei" },
        new Country { Id = 3, Name = "Cambodia", Code = "KH", Currency = "KHR", CapitalCity = "Phnom Penh" },
        // ... add more countries
    };
    public IEnumerable<Country> GetAllCountries() => _countries;
    public Country GetCountryById(int id) => _countries.FirstOrDefault(c => c.Id == id);
    // Implement other CRUD methods as needed
}