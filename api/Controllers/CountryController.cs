using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/[controller]")]
public class CountryController : ControllerBase {
    private readonly ICountryService _countryService;
    public CountryController(ICountryService countryService) {
        _countryService = countryService;
    }
    [HttpGet]
    public IEnumerable<Country> Get() => _countryService.GetAllCountries();
    [HttpGet("{id}")]
    public Country Get(int id) => _countryService.GetCountryById(id);
}